//
//  DetailedAlbumViewController.swift
//  fora-test
//
//  Created by George Kazhuro on 20.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class DetailedAlbumViewController: UIViewController, DetailedAlbumModule {
  
  let disposeBag = DisposeBag()
  
  @IBOutlet weak var tableView: UITableView!  {
    didSet {
      tableView.register(TrackTableViewCell.self, forCellReuseIdentifier: "TrackTableViewCell")
      let cellNib = UINib(nibName: "TrackTableViewCell", bundle: nil)
      tableView.register(cellNib, forCellReuseIdentifier: "TrackTableViewCell")
    }
  }
  
  var viewModel: DetailedAlbumModuleViewModel!
  
  // TableViewHeader, отображающий информацию об альбоме
  
  lazy var headerView: DetailedAlbumHeaderView = {
    return DetailedAlbumHeaderView.instanceFromNib()
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    headerView.frame.size = CGSize(width: tableView.bounds.width, height: 200)
    
    // Отобразить информацию об альбоме, когда она будет готова
    
    viewModel.albumInfoObservable
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { [weak self] (album) in
        guard let headerView = self?.headerView else { return }
        headerView.configure(with: album)
        self?.tableView.tableHeaderView = headerView
      }).disposed(by: disposeBag)
    
    // Отобразить список треков, когда они будут получены
    
    viewModel.songsObservable
    .bind(to: tableView.rx.items(cellIdentifier: "TrackTableViewCell",
                                 cellType: TrackTableViewCell.self)) { row, element, cell in
                                  cell.configure(with: element) }
    .disposed(by: disposeBag)
  }
}
