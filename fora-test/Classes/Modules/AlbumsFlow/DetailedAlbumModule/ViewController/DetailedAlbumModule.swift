//
//  DetailedAlbumModule.swift
//  fora-test
//
//  Created by George Kazhuro on 22.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

protocol DetailedAlbumModule: Presentable {
  var viewModel: DetailedAlbumModuleViewModel! { get }
}
