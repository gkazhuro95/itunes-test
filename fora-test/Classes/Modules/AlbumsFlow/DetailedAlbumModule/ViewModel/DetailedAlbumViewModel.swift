//
//  DetailedAlbumViewModel.swift
//  fora-test
//
//  Created by George Kazhuro on 20.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation
import RxSwift

class DetailedAlbumViewModel: DetailedAlbumModuleViewModel {
  
  // Ответ сервера содержащий список треков для альбома
  typealias TrackListSearchResponse = SearchResponse<Track>
  
  private let album: Album
  private let dispatcher: NetworkDispatcher
  
  // Слушатель информации об альбоме
  private let albumInfoSubject: BehaviorSubject<Album>
  
  // Слушатель, сообщающий, что информация об альбоме готова к отображению
  var albumInfoObservable: Observable<Album>
  
  // Слушатель полученных треков с альбома
  var songsObservable: Observable<[Track]>

  init(album: Album, viewIsReady: Observable<Void>) {
    let dispatcher = NetworkDispatcher()
    
    self.album = album
    
    albumInfoSubject = BehaviorSubject<Album>(value: album)
    
    // Сообщить, когда будет вызван viewDidLoad и установлены данные об альбоме
    albumInfoObservable = Observable.combineLatest(viewIsReady, albumInfoSubject) { _, album in
      return album
    }
    
    songsObservable = albumInfoObservable.flatMapLatest({ (albumNext) -> Observable<Data> in
      return dispatcher.rx.response(ApiService.lookupAlbumDetailed(id: albumNext.collectionId))
    })
      .decode(type: TrackListSearchResponse.self)
      .map { $0.results.elements }
      .catchErrorJustReturn([])
    
    self.dispatcher = dispatcher
  }
}
