//
//  DetailedAlbumModuleViewModel.swift
//  fora-test
//
//  Created by George Kazhuro on 22.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import RxSwift

// Протокол описывающий ViewModel, содержащую информцию об альбоме и список треков

protocol DetailedAlbumModuleViewModel {
  // OUTPUT
  var albumInfoObservable: Observable<Album> { get }
  var songsObservable: Observable<[Track]> { get }
}
