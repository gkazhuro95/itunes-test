//
//  DetailedAlbumModuleAssembly.swift
//  fora-test
//
//  Created by George Kazhuro on 20.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation
import RxSwift

class DetailedAlbumModuleAssembly {
  func assemble(album: Album) -> DetailedAlbumModule {
    let viewController = DetailedAlbumViewController(nibName: "DetailedAlbumViewController", bundle: nil)
    let viewModel = DetailedAlbumViewModel(album: album,
                                           viewIsReady: viewController.rx.viewDidLoad.asObservable())
    viewController.viewModel = viewModel
    return viewController
  }
}
