//
//  SearchAlbumsCoordinator.swift
//  fora-test
//
//  Created by George Kazhuro on 19.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

protocol AlbumsListCoordinator: Coordinator { }

class AlbumsListCoordinatorObj: BaseCoordinator, AlbumsListCoordinator {
  override func start() {
    let albumsListModule = AlbumsListModuleAssembly().assemble()
    router.setRootModule(albumsListModule, animated: false)
    albumsListModule.viewModel.albumSelected.subscribe(onNext: { [weak self] (album) in
      self?.showDetailedAlbumModule(album) }).disposed(by: disposeBag)
  }
  
  func showDetailedAlbumModule(_ album: Album) {
    print(album.collectionId)
    let detailedAlbumModule = DetailedAlbumModuleAssembly().assemble(album: album)
    router.push(detailedAlbumModule, animated: true)
  }
}
