//
//  AlbumListModule.swift
//  fora-test
//
//  Created by George Kazhuro on 20.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import RxSwift

// Сборка модуля AlbumsList

class AlbumsListModuleAssembly {
  func assemble() -> AlbumsListModule {
    let viewController = AlbumsListViewController(nibName: "AlbumsListViewController", bundle: nil)
    viewController.viewModel = AlbumsListViewModel()
    return viewController
  }
}
