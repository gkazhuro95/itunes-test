//
//  AlbumsListViewController.swift
//  fora-test
//
//  Created by George Kazhuro on 18.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class AlbumsListViewController: UIViewController, AlbumsListModule {
  
  var searchController: UISearchController!
  var searchBar: UISearchBar!
  
  @IBOutlet weak var collectionView: UICollectionView! {
    didSet {
      collectionView.delegate = self
      collectionView.register(AlbumCollectionViewCell.self,
                              forCellWithReuseIdentifier: "AlbumCollectionViewCell")
      let cellNib = UINib(nibName: "AlbumCollectionViewCell", bundle: nil)
      collectionView.register(cellNib, forCellWithReuseIdentifier: "AlbumCollectionViewCell")
    }
  }
  
  let disposeBag = DisposeBag()
  var viewModel: AlbumsListModuleViewModel!
  
  // View, показывающее статус загрузки и сообщения об ошибку
  
  var placeholderView: PlaceholderView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupSearchBar()
    
    let backgroundView = PlaceholderView()
    collectionView.backgroundView = backgroundView
    
    // Отслеживает изменения введенные пользователем с задержкой 0.5 сек.
    
    let searchBarChanged = searchBar.rx.text
    .orEmpty
    .filter { $0.count > 0 }
    .debounce(0.5, scheduler: MainScheduler.instance)
    .distinctUntilChanged()
    
    // Статус загрузки, в случае если true, отобразить UIActivityIndicator
    
    viewModel.isLoading.subscribe(onNext: { (result) in
      if result {
        backgroundView.isHidden = false
        backgroundView.startLoading()
      }
    }).disposed(by: disposeBag)
    
    // В случае ошибки, отобразить сообщение на PlaceholderView
    
    viewModel.errorMessage
      .observeOn(MainScheduler.instance)
      .subscribe(onNext: { (error) in
        backgroundView.isHidden = false
        backgroundView.setPlaceholderText(text: error)
      }).disposed(by: disposeBag)
    
    // Биндинг пришедших альбомов на UICollectionVIew
    
    viewModel.albums
      .observeOn(MainScheduler.instance)
      .do(onNext: { _ in
        backgroundView.isHidden = true
      })
      .bind(to: collectionView.rx.items(cellIdentifier: "AlbumCollectionViewCell",
                                      cellType: AlbumCollectionViewCell.self)) { row, element, cell in
      let url = URL(string: element.artworkUrl100)!
      cell.coverImageView.kf.setImage(with: url) }
      .disposed(by: disposeBag)
    
    // Биндинг введенных пользователем данных на свойство ViewModel
    
    searchBarChanged.bind(to: viewModel.albumNameQuery).disposed(by: disposeBag)
    
    // Биндинг выбранного альбома на свойство ViewModel
    
    collectionView.rx.modelSelected(Album.self).asObservable().bind(to: viewModel.albumSelected).disposed(by: disposeBag)
    
    // Скрыть клавиатуру когда происходит скроллинг, либо при выходе с экрана
    
    Observable.merge(collectionView.rx.contentOffset.map {_ in return ()},
                     rx.viewWillDisappear.map {_ in return ()})
      .subscribe(onNext: { [weak self] _ in
        self?.hideKeyboard()
      })
      .disposed(by: disposeBag)
  }
  
  private func hideKeyboard() {
    if searchBar.isFirstResponder {
      _ = searchBar.resignFirstResponder()
    }
  }
  
  private func setupSearchBar() {
    searchController = UISearchController(searchResultsController:  nil)
    searchBar = searchController.searchBar
    searchController.hidesNavigationBarDuringPresentation = false
    searchController.dimsBackgroundDuringPresentation = false
    self.navigationItem.titleView = searchController.searchBar
  }
}

extension AlbumsListViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let cellWidth = collectionView.bounds.width / 4
    return CGSize(width: collectionView.bounds.width / 4, height: cellWidth)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
}
