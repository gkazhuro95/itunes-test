//
//  AlbumListModule.swift
//  fora-test
//
//  Created by George Kazhuro on 21.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

protocol AlbumsListModule: Presentable {
  var viewModel: AlbumsListModuleViewModel! { get }
}
