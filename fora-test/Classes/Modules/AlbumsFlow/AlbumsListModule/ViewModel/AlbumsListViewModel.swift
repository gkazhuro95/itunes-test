//
//  AlbumsListViewModel.swift
//  fora-test
//
//  Created by George Kazhuro on 18.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AlbumsListViewModel: AlbumsListModuleViewModel {
  
  // Ответ сервера содержащий список альбомов
  typealias AlbumSearchResponse = SearchResponse<Album>
  
  typealias Result = NetworkResult<[Album]>
  
  private let dispatcher: NetworkDispatcher
  private let activityIndicator = ActivityIndicator()
  
  // MARK: - INPUT
  
  var albumNameQuery = PublishSubject<String>()
  var albumSelected = PublishSubject<Album>()
  
  // MARK: - OUTPUT
  
  var albums: Observable<[Album]>
  var errorMessage: Observable<String>
  var isLoading: Observable<Bool> {
    return activityIndicator.asObservable()
  }
  
  // MARK: - INIT

  init() {
    let dispatcher = NetworkDispatcher()
    
    // Слушатель ввода пользователя, на каждый пришедший String параметр формируется запрос,
    // результатом которого является NetworkResult, содержащий массив альбомов или ошибку
    
    let albumsResult = albumNameQuery.filter { $0.count > 0 }
      .flatMapLatest({ term -> Observable<Result> in
        return dispatcher.rx.response(ApiService.searchAlbums(term: term))
          .decode(type: AlbumSearchResponse.self)
          .map { return Result.success($0.results.elements) }
          .catchError({ (error) -> Observable<Result> in
            return Observable.just(Result.failure(error))
          })
      }).share()
    
    // Список альбомов, полученный из запроса
    
    albums = albumsResult
      .filter { $0.value != nil }
      .map { $0.value! }
      .map { $0.sorted() }.share()
    
    // Сообщение об ошибке, пришедшее из запроса
    
    errorMessage = albumsResult.filter { $0.error != nil }
      .map { $0.error!.localizedDescription }
    
    self.dispatcher = dispatcher
  }
}
