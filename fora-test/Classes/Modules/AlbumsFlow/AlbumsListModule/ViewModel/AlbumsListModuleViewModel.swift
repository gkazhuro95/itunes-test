//
//  AlbumsListModuleViewModel.swift
//  fora-test
//
//  Created by George Kazhuro on 21.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import RxSwift

protocol AlbumsListModuleViewModel {
  // INPUT
  var albumSelected: PublishSubject<Album> { get }
  var albumNameQuery: PublishSubject<String> { get }
  
  // OUTPUT
  var albums: Observable<[Album]> { get }
  var errorMessage: Observable<String> { get }
  var isLoading: Observable<Bool> { get }
}
