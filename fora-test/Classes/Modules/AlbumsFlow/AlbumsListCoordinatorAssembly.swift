//
//  AlbumsListAssembler.swift
//  fora-test
//
//  Created by George Kazhuro on 19.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import UIKit

struct AlbumsListCoordinatorAssembly {
  func assemble() -> AlbumsListCoordinator {
    let navigationController = UINavigationController()
    let router = NavigationRouter(rootController: navigationController)
    let coordinator = AlbumsListCoordinatorObj(router: router)
    return coordinator
  }
}
