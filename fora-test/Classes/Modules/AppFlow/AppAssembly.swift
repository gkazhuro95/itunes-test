//
//  AppAssembly.swift
//  fora-test
//
//  Created by George Kazhuro on 21.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import UIKit

// Сборка AppCoordinator включающая в себя основной роутер приложения

class AppAssembly {
  func assemble(window: UIWindow?) -> AppCoordinator {
    let router = AppRouter(window: window)
    return AppCoordinatorObj(router: router)
  }
}
