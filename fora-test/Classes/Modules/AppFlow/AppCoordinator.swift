//
//  AppCoordinator.swift
//  fora-test
//
//  Created by George Kazhuro on 19.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

protocol AppCoordinator: Coordinator { }

class AppCoordinatorObj: BaseCoordinator, AppCoordinator {
  
  // Начало работы и выбор стартового flow
  
  override func start() {
    runSearchAlbumFlow()
  }
  
  // Начало работы flow с поиском альбомов и установка его как корневого модуля
  
  func runSearchAlbumFlow() {
    let coordinator = AlbumsListCoordinatorAssembly().assemble()
    router.setRootModule(coordinator.router, animated: false)
    addDependency(coordinator)
    coordinator.start()
  }
}
