//
//  Task+Rx.swift
//  fora-test
//
//  Created by George Kazhuro on 18.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation
import RxSwift

extension NetworkDispatcher: ReactiveCompatible {}

extension Reactive where Base: Dispatcher {
  public func response(_ request: Request) -> Observable<Data> {
    return Observable.create { [weak base] observer in
      
      print("Request \(request.parameters) started")
      
      base?.execute(request: request, completion: { (data, error) in
        guard let data = data else {
          print("Request \(request.parameters) erorr")
          observer.on(.error(error ?? NetworkErrors.noData))
          return
        }
        
        print("Request \(request.parameters) finished")
        
        observer.on(.next(data))
        observer.onCompleted()
      })
      
      return Disposables.create {
        print("Request \(request.parameters) disposed")
        base?.cancel()
      }
    }
  }
}
