//
//  Decoder+Rx.swift
//  fora-test
//
//  Created by George Kazhuro on 21.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation
import RxSwift

public extension ObservableType where E == Data {
  func decode<T>(type: T.Type) -> Observable<T> where T: Decodable {
    return self.flatMap {
      return Observable.just(try JSONDecoder().decode(T.self, from: $0))
    }
  }
}
