//
//  Response.swift
//  fora-test
//
//  Created by George Kazhuro on 18.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Результат поиска в iTunes
// Поскольку поле results содержит элементы разного типа, было решено оставить только те, которые соответствуют типу T. В другом случае пришлось бы использовать AnyObject и бесконечный беспощадный кастинг элементов

struct SearchResponse<T: Decodable>: Decodable {
  let resultCount: Int
  let results: FailableCodableArray<T>
}
