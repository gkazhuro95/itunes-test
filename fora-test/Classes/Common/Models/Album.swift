//
//  Album.swift
//  fora-test
//
//  Created by George Kazhuro on 18.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

class Album: Media {
  private enum CodingKeys: String, CodingKey {
    case artworkUrl60
    case artworkUrl100
    case collectionId
    case artistName
    case collectionName
    case primaryGenreName
    case releaseDate
  }
  
  let artworkUrl60: String
  let artworkUrl100: String
  let collectionId: Int
  let artistName: String
  let collectionName: String
  let primaryGenreName: String
  let releaseDate: String
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    artworkUrl60 = try container.decode(String.self, forKey: .artworkUrl60)
    artworkUrl100 = try container.decode(String.self, forKey: .artworkUrl100)
    collectionId = try container.decode(Int.self, forKey: .collectionId)
    artistName = try container.decode(String.self, forKey: .artistName)
    collectionName = try container.decode(String.self, forKey: .collectionName)
    primaryGenreName = try container.decode(String.self, forKey: .primaryGenreName)
    releaseDate = try container.decode(String.self, forKey: .releaseDate)
    try super.init(from: decoder)
  }
}

extension Album: Comparable {
  static func == (lhs: Album, rhs: Album) -> Bool {
    return lhs.collectionName == rhs.collectionName
  }
  
  static func < (lhs: Album, rhs: Album) -> Bool {
    return lhs.collectionName < rhs.collectionName
  }
}
