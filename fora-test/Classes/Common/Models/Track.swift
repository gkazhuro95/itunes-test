//
//  Track.swift
//  fora-test
//
//  Created by George Kazhuro on 20.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

class Track: Media {
  private enum CodingKeys: String, CodingKey {
    case trackName
    case trackNumber
  }
  
  let trackName: String
  let trackNumber: Int
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    trackName = try container.decode(String.self, forKey: .trackName)
    trackNumber = try container.decode(Int.self, forKey: .trackNumber)
    try super.init(from: decoder)
  }
}
