//
//  Media.swift
//  fora-test
//
//  Created by George Kazhuro on 20.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

class Media: Decodable {
  private enum CodingKeys: String, CodingKey {
    case wrapperType = "wrapperType"
  }
  
  let wrapperType: String
  
  required init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    wrapperType = try values.decode(String.self, forKey: .wrapperType)
  }
}
