//
//  PlaceholderView.swift
//  fora-test
//
//  Created by George Kazhuro on 20.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import UIKit

class PlaceholderView: UIView {
  
  lazy var placeholderInfoLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.font = UIFont.systemFont(ofSize: 17)
    label.textAlignment = .center
    label.textColor = .lightGray
    label.translatesAutoresizingMaskIntoConstraints = false
    label.isHidden = true
    return label
  }()
  
  lazy var placeholderActivityIndicator: UIActivityIndicatorView = {
    let activityIndicatorView = UIActivityIndicatorView()
    activityIndicatorView.activityIndicatorViewStyle = .gray
    activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
    activityIndicatorView.isHidden = true
    return activityIndicatorView
  }()
  
  override func didMoveToSuperview() {
    guard let superview = superview else { return }
    
    addSubview(placeholderInfoLabel)
    addSubview(placeholderActivityIndicator)
    
    placeholderActivityIndicator.centerXAnchor.constraint(equalTo: superview.centerXAnchor).isActive = true
    placeholderActivityIndicator.centerYAnchor.constraint(equalTo: superview.centerYAnchor).isActive = true
    
    placeholderInfoLabel.widthAnchor.constraint(equalTo: superview.widthAnchor, multiplier: 0.8).isActive = true
    placeholderInfoLabel.centerXAnchor.constraint(equalTo: superview.centerXAnchor).isActive = true
    placeholderInfoLabel.centerYAnchor.constraint(equalTo: superview.centerYAnchor).isActive = true
  }
  
  func startLoading() {
    placeholderActivityIndicator.isHidden = false
    placeholderActivityIndicator.startAnimating()
  }
  
  func setPlaceholderText(text: String) {
    placeholderInfoLabel.text = text
    placeholderActivityIndicator.isHidden = true
    placeholderActivityIndicator.stopAnimating()
  }
}
