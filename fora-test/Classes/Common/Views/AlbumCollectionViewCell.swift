//
//  AlbumCollectionViewCell.swift
//  fora-test
//
//  Created by George Kazhuro on 18.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var coverImageView: UIImageView! {
    didSet {
      coverImageView.kf.indicatorType = .activity
    }
  }
  
  // Очень плохой способ конфигурирования ячейки, но из-за нехватки времени и простой логики
  // решил оставить его, чтобы потом в дальнейшем найти решение лучше
  
  func configure(with imagePath: String) {
    coverImageView.kf.setImage(with: URL(string:imagePath))
  }
}
