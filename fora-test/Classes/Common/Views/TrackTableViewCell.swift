//
//  TrackTableViewCell.swift
//  fora-test
//
//  Created by George Kazhuro on 20.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import UIKit

class TrackTableViewCell: UITableViewCell {
  
  @IBOutlet weak var trackNameLabel: UILabel!
  @IBOutlet weak var trackNumberLabel: UILabel!
  
  // Очень плохой способ конфигурирования ячейки, но из-за нехватки времени и простой логики
  // решил оставить его, чтобы потом в дальнейшем найти решение лучше
  
  func configure(with track: Track) {
    trackNameLabel.text = track.trackName
    trackNumberLabel.text = "\(track.trackNumber)."
  }
  
}
