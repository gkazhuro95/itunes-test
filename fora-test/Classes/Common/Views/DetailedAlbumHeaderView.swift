//
//  DetailedAlbumHeaderView.swift
//  fora-test
//
//  Created by George Kazhuro on 20.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import UIKit

class DetailedAlbumHeaderView: UIView {
  @IBOutlet weak var coverImageView: UIImageView!
  @IBOutlet weak var albumTitleLabel: UILabel!
  @IBOutlet weak var albumArtistLabel: UILabel!
  @IBOutlet weak var genreLabel: UILabel!
  @IBOutlet weak var yearLabel: UILabel!
  
  // Очень плохой способ конфигурирования представления, но из-за нехватки времени и простой логики
  // решил оставить его, чтобы потом в дальнейшем найти решение лучше
  
  func configure(with album: Album) {
    let url = URL(string: album.artworkUrl100)
    coverImageView.kf.setImage(with: url)
    albumTitleLabel.text = album.collectionName
    albumArtistLabel.text = album.artistName
    genreLabel.text = album.primaryGenreName
  }
  
  class func instanceFromNib() -> DetailedAlbumHeaderView {
    return UINib(nibName: "DetailedAlbumHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! DetailedAlbumHeaderView
  }
}
