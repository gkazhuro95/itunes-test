//
//  AppRouter.swift
//  fora-test
//
//  Created by George Kazhuro on 19.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import UIKit

// Имплементация роутера для UIViewController

final class AppRouter: Routable {
  
  func present(_ module: Presentable?, animated: Bool) {
    guard let viewControllerToPresent = module?.toPresent else { return }
    rootController?.present(viewControllerToPresent, animated: animated, completion: nil)
  }
  
  func push(_ module: Presentable?, animated: Bool) {
    guard let viewControllerToPush = module?.toPresent else { return }
    (rootController as? UINavigationController)?.pushViewController(viewControllerToPush, animated: animated)
  }
  
  func dismissModule(animated: Bool, completion: (() -> Void)?) {
    rootController?.dismiss(animated: animated, completion: nil)
  }
  
  func popModule(animated: Bool) {
    (rootController as? UINavigationController)?.popViewController(animated: true)
  }
  
  func setRootModule(_ module: Presentable?, animated: Bool) {
    guard let toPresent = module?.toPresent else { return }
    self.rootController = toPresent
  }
  
  // MARK: Presentable
  var toPresent: UIViewController? {
    return rootController
  }
  
  private var rootController: UIViewController? {
    get {
      return window?.rootViewController
    } set {
      window?.rootViewController = newValue
    }
  }
  private weak var window: UIWindow?
  
  init(window: UIWindow?) {
    self.window = window
  }
  
  
}
