//
//  BaseCoordinator.swift
//  fora-test
//
//  Created by George Kazhuro on 19.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation
import RxSwift

open class BaseCoordinator: Coordinator {
  
  var childCoordinators: [Coordinator] = []
  
  let router: Routable
  
  let disposeBag = DisposeBag()
  
  init(router: Routable) {
    self.router = router
  }
  
  open func start() { }
  
  func addDependency(_ coordinator: Coordinator) {
    guard !childCoordinators.contains(where: { $0 === coordinator })
      else { return }
    childCoordinators.append(coordinator)
  }
  
  func removeDependency(_ coordinator: Coordinator?) {
    guard let indexToRemove = childCoordinators.index(where: { $0 === coordinator })
      else { return }
    childCoordinators.remove(at: indexToRemove)
  }
  
  func removeAllDependencies() {
    childCoordinators.removeAll()
  }
}
