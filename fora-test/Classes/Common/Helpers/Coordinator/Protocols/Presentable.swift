//
//  Presentable.swift
//  fora-test
//
//  Created by George Kazhuro on 19.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import UIKit

// Протокол, которым закрываются все UIViewController'ы, позволяющий производить переход на них из роутера

protocol Presentable {
  var toPresent: UIViewController? { get }
}

extension UIViewController: Presentable {
  var toPresent: UIViewController? {
    return self
  }
}
