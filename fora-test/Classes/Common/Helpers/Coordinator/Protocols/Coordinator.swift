//
//  Coordinator.swift
//  fora-test
//
//  Created by George Kazhuro on 19.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Для переключения между модулями используется координатор

protocol Coordinator: class {
  
  // Роутер отвечает за переход между экранами
  
  var router: Routable { get }
  
  // Метод начала работы модуля, в котором происходит сборка основных компонентов
  
  func start()
}
