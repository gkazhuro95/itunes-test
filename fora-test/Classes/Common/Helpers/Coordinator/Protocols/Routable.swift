//
//  Routable.swift
//  fora-test
//
//  Created by George Kazhuro on 19.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Протокол описывающий переходы между экранами, можно работать как с UIViewController,
// так и с UINavigationController

protocol Routable: Presentable {
  func present(_ module: Presentable?, animated: Bool)
  func push(_ module: Presentable?, animated: Bool)
  func dismissModule(animated: Bool, completion: (() -> Void)?)
  func popModule(animated: Bool)
  func setRootModule(_ module: Presentable?, animated: Bool)
}
