//
//  FailableDecodableArray.swift
//  fora-test
//
//  Created by George Kazhuro on 20.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Структура которая занимается декодингом массивов, которые могут содержать Failable элементы
// К примеру, требуется получить массив, содержащий Tracks, однако один из элементов оказывается элемент, не совпадающий с типом всего массива. В таком случае этот элемент будет исключен

struct FailableCodableArray<Element:Decodable> : Decodable {
  
  var elements: [Element]
  
  init(from decoder: Decoder) throws {
    
    var container = try decoder.unkeyedContainer()
    var elements = [Element]()
    
    if let count = container.count {
      elements.reserveCapacity(count)
    }
    
    while !container.isAtEnd {
      if let element = try container.decode(FailableDecodable<Element>.self).base {
        elements.append(element)
      }
    }
    
    self.elements = elements
  }
}
