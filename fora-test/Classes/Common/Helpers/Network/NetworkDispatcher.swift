//
//  NetworkDispatcher.swift
//  network-app
//
//  Created by George Kazhuro on 31.05.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Имплементация Dispatcher с использованием URLSession

class NetworkDispatcher: Dispatcher {
  
  private var task: URLSessionTask?
  
  func execute(request: Request, completion: @escaping NetworkDispatcherCompletion) {
    let session = URLSession.shared
    do {
      let request = try self.prepareURLRequest(for: request)
      task = session.dataTask(with: request) { (data, response, error) in
        completion(data, error)
      }
    } catch {
      completion(nil, error)
    }
    self.task?.resume()
  }
  
  func cancel() {
    task?.cancel()
  }
  
  // Метро подготавлювающий URL с использованием параметров сформированных в Request
  
  private func prepareURLRequest(for request: Request) throws -> URLRequest {
    let urlPath = "\(request.baseURL)/\(request.path)"
    guard let url = URL(string: urlPath) else { throw NetworkErrors.badPath }
    var urlRequest = URLRequest(url: url,
                             cachePolicy: .reloadIgnoringLocalCacheData,
                             timeoutInterval: 10.0)
    urlRequest.httpMethod = request.method.rawValue
    
    switch request.parameters {
    case .body(let params):
      if let params = params as? [String: String] {
        urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: .init(rawValue: 0))
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
      } else {
        throw NetworkErrors.badInput
      }
    case .url(let params):
      if let params = params {
        let queryParams = params.map({ (element) -> URLQueryItem in
          return URLQueryItem(name: element.key, value: "\(element.value)")
        })
        guard var components = URLComponents(string: urlPath) else {
          throw NetworkErrors.badInput
        }
        components.queryItems = queryParams
        urlRequest.url = components.url
      } else {
        throw NetworkErrors.badInput
      }
    }
    return urlRequest
  }
  
}
