//
//  HTTPMethod.swift
//  network-app
//
//  Created by George Kazhuro on 31.05.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Типы запросов к серверу

public enum HTTPMethod: String {
  case post = "POST"
  case get = "GET"
}
