//
//  NetworkResult.swift
//  fora-test
//
//  Created by George Kazhuro on 21.06.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Результат выполнения запроса, может быть завершен успешно success(T)
// и содержать полученный параметр, либо
// в случае неудачи содержит ошибку failure(Error)

enum NetworkResult<T> {
  case success(T)
  case failure(Error)
  
  public var error: Error? {
    switch self {
    case .success(_):
      return nil
    case let .failure(error):
      return error
    }
  }
  
  public var value: T? {
    switch self {
    case let .success(value):
      return value
    case .failure(_):
      return nil
    }
  }
  
  public func dematerialize() throws -> T {
    switch self {
    case let .success(value):
      return value
    case let .failure(error):
      throw error
    }
  }
}
