//
//  Request.swift
//  network-app
//
//  Created by George Kazhuro on 31.05.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Этот протокол описывает основную структуру запроса

public protocol Request {
  
  // Основная ссылка, по которой располагается ресурс
  
  var baseURL: String { get }
  
  // URL путь к ресурсу
  
  var path: String { get }
  
  // Метод запроса к серверу
  
  var method: HTTPMethod { get }
  
  // Способ передачи параметров серверу
  
  var parameters: RequestParameters { get }
}
 
