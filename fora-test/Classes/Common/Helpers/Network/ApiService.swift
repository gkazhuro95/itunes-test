//
//  ApiService.swift
//  network-app
//
//  Created by George Kazhuro on 31.05.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Сервис, описывающий все запросы к сети. Идея позаимствованна из Moya

public enum ApiService {
  case searchAlbums(term: String)
  case lookupAlbumDetailed(id: Int)
}

extension ApiService: Request {
  public var baseURL: String {
    return "https://itunes.apple.com"
  }
  
  public var path: String {
    switch self {
    case .searchAlbums(_):
      return "search"
    case .lookupAlbumDetailed(_):
      return "lookup"
    }
  }
  
  public var method: HTTPMethod {
    switch self {
    case .searchAlbums(_), .lookupAlbumDetailed(_):
      return .get
    }
  }
  
  public var parameters: RequestParameters {
    switch self {
    case .searchAlbums(let term):
      return .url(["entity": "album",
                   "media": "music",
                   "term": term])
    case .lookupAlbumDetailed(let id):
      return .url(["id": id,
                   "entity": "song"])
    }
  }
}
