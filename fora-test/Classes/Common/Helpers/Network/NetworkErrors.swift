//
//  NetworkErrors.swift
//  network-app
//
//  Created by George Kazhuro on 31.05.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Ошибки возможные при работе с сетью

public enum NetworkErrors: Error {
  case badPath
  case badInput
  case noData
  case decodingError
}
