//
//  RequestParameters.swift
//  network-app
//
//  Created by George Kazhuro on 31.05.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

// Способы передачи параметров серверу

public enum RequestParameters {
  
  // В теле запроса
  
  case body(_ : [String: Any]?)
  
  // В URL
  
  case url(_ : [String: Any]?)
}
