//
//  Dispatcher.swift
//  network-app
//
//  Created by George Kazhuro on 31.05.2018.
//  Copyright © 2018 George Kazhuro. All rights reserved.
//

import Foundation

public typealias NetworkDispatcherCompletion = (_ data: Data?, _ error: Error?) -> ()

// Протокол, описывающий запросы к сети. Можно использововать как с URLSession,
// так в случае необходимости и с Alamofire

public protocol Dispatcher: class {
  
  // Выполнение запроса к сети, с параметром Request и escaping блок, содержащий данные,
  // либо ошибку
  
  func execute(request: Request, completion: @escaping NetworkDispatcherCompletion)
  
  // Отмена запущенной сессии
  
  func cancel()
}
